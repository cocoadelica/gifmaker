//
//  GIFController.swift
//  GIFMaker
//
//  Created by Rob Stearn on 17/06/2014.
//  Copyright (c) 2014 Cocoadelica. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation

enum CameraCaptureDevice {
  case Front
  case Rear
}

class GIFController {
  var captureDevice: CameraCaptureDevice?
  var captureSession: AVCaptureSession?
  
  init(captureDevice: CameraCaptureDevice){
    self.captureDevice = captureDevice
    self.captureSession = AVCaptureSession()
  }
  
  func setupCaptureSession() {
    let availableDevices = AVCaptureDevice.devices()
    var captureDeviceInput: AVCaptureDeviceInput?
    let isFrontDevice: Bool = (self.captureDevice == CameraCaptureDevice.Front)
    for device : AnyObject in availableDevices {
      if (isFrontDevice && device.supportsAVCaptureSessionPreset(AVCaptureSessionPresetMedium)) {
        self.captureSession!.beginConfiguration()
        let eachDevice = device as AVCaptureDevice
        eachDevice.lockForConfiguration(nil)
        if eachDevice.isFocusModeSupported(AVCaptureFocusMode.ContinuousAutoFocus) {
          eachDevice.focusMode = AVCaptureFocusMode.AutoFocus
        }
        eachDevice.unlockForConfiguration()
        self.captureSession!.commitConfiguration()
        var deviceError: NSError?
        captureDeviceInput = (AVCaptureDeviceInput.deviceInputWithDevice(eachDevice, error: &deviceError) as AVCaptureDeviceInput)
        break
      }
    }
    self.captureSession!.sessionPreset = AVCaptureSessionPresetPhoto
    self.captureSession!.addInput(captureDeviceInput)
    if captureDeviceInput {
      self.captureSession!.startRunning()
    }
  }
}
