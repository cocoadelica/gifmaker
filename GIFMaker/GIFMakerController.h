//
//  GIFMakerController.h
//  GIFMaker
//
//  Created by Rob Stearn on 10/06/2014.
//  Copyright (c) 2014 Cocoadelica. All rights reserved.
//

@import Foundation;

typedef NS_ENUM(NSInteger, CAPTURE_DEVICE) {
  CAPTURE_DEVICE_FRONT = 0,
  CAPTURE_DEVICE_BACK
};

@interface GIFMakerController : NSObject
@property (nonatomic, readonly) CAPTURE_DEVICE selectedCaptureDevice;
- (instancetype)initWithCaptureDevice:(CAPTURE_DEVICE)selectedCaptureDevice NS_DESIGNATED_INITIALIZER;
@end
