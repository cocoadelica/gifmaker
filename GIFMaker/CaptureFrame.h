//
//  CaptureFrame.h
//  GIFMaker
//
//  Created by Rob Stearn on 11/06/2014.
//  Copyright (c) 2014 Cocoadelica. All rights reserved.
//

@import Foundation;
@import AVFoundation;
@class SwiftFrame;

typedef NS_ENUM(NSInteger, CAPTURE_DEVICE) {
  CAPTURE_DEVICE_BACK = 0,
  CAPTURE_DEVICE_FRONT = 1,
};

@interface CaptureFrame : NSObject

@property (nonatomic, readonly) CAPTURE_DEVICE captureDevice;

- (instancetype)initWithCaptureDevice:(CAPTURE_DEVICE)chosenCaptureDevice forSession:(AVCaptureSession *)session;
- (SwiftFrame *)captureFrame;

@end
