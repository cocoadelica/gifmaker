//
//  Frame.h
//  GIFMaker
//
//  Created by Rob Stearn on 10/06/2014.
//  Copyright (c) 2014 Cocoadelica. All rights reserved.
//

@import Foundation;

@interface Frame : NSObject <NSCoding>

@property (nonatomic, strong, readonly) NSURL *frameFilePath;
@property (nonatomic, strong, readonly) NSDate *frameCaptureDate;

- (instancetype)initWithFrameFilePath:(NSURL *)pathToFrame andCaptureDate:(NSDate *)captureDate;

@end
