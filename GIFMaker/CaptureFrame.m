//
//  CaptureFrame.m
//  GIFMaker
//
//  Created by Rob Stearn on 11/06/2014.
//  Copyright (c) 2014 Cocoadelica. All rights reserved.
//

#import "CaptureFrame.h"
#import "GIFMaker/GIFMaker-Swift.h"
@import AVFoundation;

@interface CaptureFrame ()
@property (nonatomic, strong) AVCaptureSession *session;
@property (nonatomic, strong) AVCaptureStillImageOutput *stillImageOutput;
@end

@implementation CaptureFrame

#pragma mark - Public

- (instancetype)initWithCaptureDevice:(CAPTURE_DEVICE)chosenCaptureDevice forSession:(AVCaptureSession *)session {
  self = [super init];
  if (self) {
    _captureDevice = chosenCaptureDevice;
    _session = session;
  }
  return self;
}

- (SwiftFrame *)captureFrame {
  return nil;
}

#pragma mark - Private

- (void)setupCapture {
  self.stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
  
  NSDictionary *outputSettings = @{AVVideoCodecKey:AVVideoCodecJPEG};
  [self.stillImageOutput setOutputSettings:outputSettings];
  
  [self.session addOutput:self.stillImageOutput];
}

//test for device availability

//configure session

//capture frame

//wrap in object and return



@end
