//
//  Frame.m
//  GIFMaker
//
//  Created by Rob Stearn on 10/06/2014.
//  Copyright (c) 2014 Cocoadelica. All rights reserved.
//

#import "Frame.h"

@interface Frame ()

@property (nonatomic, strong) NSURL *frameFilePath;
@property (nonatomic, strong) NSDate *frameCaptureDate;

@end

@implementation Frame

- (instancetype)initWithFrameFilePath:(NSURL *)pathToFrame andCaptureDate:(NSDate *)captureDate {
  self = [super init];
  if (self) {
    _frameFilePath = pathToFrame;
    _frameCaptureDate = captureDate;
  }
  return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
  self = [super init];
  if (self) {
    _frameCaptureDate = [coder decodeObjectForKey:@"GIFFrameCaptureDate"];
    _frameFilePath = [NSURL URLWithString:[coder decodeObjectForKey:@"GIFFrameFilePath"]];
  }
  return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
  [aCoder encodeObject:self.frameFilePath.path forKey:@"GIFFrameFilePath"];
  [aCoder encodeObject:self.frameCaptureDate forKey:@"GIFFrameCaptureDate"];
}

@end
