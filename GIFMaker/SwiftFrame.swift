//
//  SwiftFrame.swift
//  GIFMaker
//
//  Created by Rob Stearn on 11/06/2014.
//  Copyright (c) 2014 Cocoadelica. All rights reserved.
//

import UIKit

class SwiftFrame <NSCoding> {
  
  var frameFilePath: NSURL
  var frameCaptureDate: NSDate
  
  init(frameFilePath: NSURL, frameCaptureDate: NSDate) {
    self.frameFilePath = frameFilePath
    self.frameCaptureDate = frameCaptureDate
  }
  
  init(coder: NSCoder) {
    self.frameCaptureDate = coder.decodeObjectForKey("GIFFrameCaptureDate") as NSDate
    self.frameFilePath = coder.decodeObjectForKey("GIFFrameFilePath") as NSURL
  }
  
  func encodeWithCoder(aCoder: NSCoder) {
    aCoder.encodeObject(self.frameCaptureDate, forKey: "GIFFrameCaptureDate")
    aCoder.encodeObject(self.frameFilePath, forKey: "GIFFrameFilePath")
  }
   
}
