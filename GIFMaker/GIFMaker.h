//
//  GIFMaker.h
//  GIFMaker
//
//  Created by Rob Stearn on 10/06/2014.
//  Copyright (c) 2014 Cocoadelica. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for GIFMaker.
FOUNDATION_EXPORT double GIFMakerVersionNumber;

//! Project version string for GIFMaker.
FOUNDATION_EXPORT const unsigned char GIFMakerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <GIFMaker/PublicHeader.h>


