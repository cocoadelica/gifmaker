//
//  GIFMakerController.m
//  GIFMaker
//
//  Created by Rob Stearn on 10/06/2014.
//  Copyright (c) 2014 Cocoadelica. All rights reserved.
//

#import "GIFMakerController.h"
@import AVFoundation;

@interface GIFMakerController ()
@property (nonatomic, strong) AVCaptureSession *session;
@property (nonatomic) CAPTURE_DEVICE selectedCaptureDevice;
@end

@implementation GIFMakerController

- (instancetype)initWithCaptureDevice:(CAPTURE_DEVICE)selectedCaptureDevice {
  self = [super init];
  if (self) {
    _selectedCaptureDevice = selectedCaptureDevice;
    _session = [[AVCaptureSession alloc] init];
    [self setupSession];
  }
  return self;
}

- (void)setupSession {
  NSArray *devices = [AVCaptureDevice devices];
  AVCaptureDeviceInput *captureDeviceInput;
  for (AVCaptureDevice *device in devices) {
    if ([device position] == (self.selectedCaptureDevice == CAPTURE_DEVICE_FRONT?AVCaptureDevicePositionFront:AVCaptureDevicePositionBack) && [device supportsAVCaptureSessionPreset:AVCaptureSessionPresetMedium]) {
      [self.session beginConfiguration];
      [device lockForConfiguration:nil];
      if ([device isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus]) {
        [device setFocusMode:AVCaptureFocusModeContinuousAutoFocus];
      }
      [device unlockForConfiguration];
      [self.session commitConfiguration];
      NSError *error = nil;
      captureDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
      break;
    }
  }
  self.session.sessionPreset = AVCaptureSessionPresetPhoto;
  [self.session addInput:captureDeviceInput];
  [self.session startRunning];
}

@end
